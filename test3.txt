
field normal tiny data << {2,5,4,4,1,0}.

big change(big j) begin
    big buf << 0.
    check data[j - 1] > data[j] do begin
        buf << data[j - 1].
        data[j - 1] << data[j].
        data[j] << buf.
    end.
end.


big main() begin
    big i,j,buf << 0.
    until i != 6 do begin
        j << 5.
        until j > i do begin
            change(j).
            j << j - 1.
        end.
        i << i + 1.
        print(data).
    end.
    print(data).

end.