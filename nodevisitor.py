import ast, copy, maze

class NodeVisitor:

    def __init__(self, error_flag, func_dict):
        self._error_flag = error_flag

        self._func_dict = func_dict

        self._scopes = {'__global' : {'_rotation' : ['unormal', 0],
                                      '_x_pos'    : ['ubig'   , 0],
                                      '_y_pos'    : ['ubig'   , 0],
                                      '_map'      : maze.map,
                                      }

                        }


        self.operators = {'-'  : lambda x, y: x -   y,
                          '+'  : lambda x, y: x +   y,
                          '/'  : lambda x, y: x //  y,
                          '*'  : lambda x, y: x *   y,
                          '<'  : lambda x, y: x <   y,
                          '>'  : lambda x, y: x >   y,
                          '<=' : lambda x, y: x <=  y,
                          '>=' : lambda x, y: x >=  y,
                          '==' : lambda x, y: x ==  y,
                          '!=' : lambda x, y: x !=  y,
                          'and': lambda x, y: x and y,
                          'or' : lambda x, y: x or  y,
                          }

        self.unary_operators = {'-': lambda x: -x,
                                '+': lambda x:  x,
                                }

        self.borders = {'tiny'  : lambda x: 0      <= x < 2,
                        'small' : lambda x: -16    <= x < 16,
                        'normal': lambda x: -512   <= x < 512,
                        'big'   : lambda x: -16384 <= x < 16384,
                        }

        self.u_borders = {'small' : lambda x: 0 <= x < 32,
                          'normal': lambda x: 0 <= x < 1024,
                          'big'   : lambda x: 0 <= x < 32768,


                          'usmall' : lambda x: 0 <= x < 32,
                          'unormal': lambda x: 0 <= x < 1024,
                          'ubig'   : lambda x: 0 <= x < 32768,
                          }

        self.field_size = {'small'  : 4,
                            'normal': 6,
                            'big'   : 13,
                            'tiny'  : 1
                           }

        self.casts = {'small' : 'normal',
                      'normal': 'big',
                      'big'   :  None,
                      'tiny'  : 'small',
                      
                      'usmall' : 'unormal',
                      'unormal': 'ubig',
                      'ubig'   :  None,
                     }

        self.robot_operators = {'go'     : self._go_op,
                                'rl'     : self._rl_op,
                                'rr'     : self._rr_op,
                                'compass': self._compass_op,
                                'sonar'  : self._sonar_op,
                                }

        self._last_pos = (0, 0, 0)
        self._sonar_calls = 0

        #print("map[9][3]", self._scopes['__global']['_map'][1][9][3])

        #self._scopes['__global']['_map'][1][12][0] = [0, 0, 1, 1, 0, 0]

        #print("next: ", self._next_cell(0, 12, 4))

    def _rotation_to_exit(self,  x_pos, y_pos, rotation):
        _map = self._scopes['__global']['_map'][1]
        assert _map[y_pos][x_pos]

        exit_cells = ()
        if x_pos == 0 and y_pos % 2 == 0:
            if y_pos == 0:
                exit_cells = (0, 1, 4, 5)
            elif y_pos == len(_map) - 1:
                exit_cells = (2, 3, 4, 5)
            else:
                exit_cells = (4, 5)
        elif x_pos == len(_map[0]) - 1 and y_pos % 2 == 1:
            if y_pos == 1:
                exit_cells = (0, 1, 2)
            elif y_pos == len(_map) - 2:
                exit_cells = (1, 2, 3)
            else:
                exit_cells = (1, 2)
        elif y_pos == 0:
            exit_cells = (0, 1, 5)
        elif y_pos == 1:
            exit_cells = [0]
        elif y_pos == len(_map) - 1:
            exit_cells = (2, 3, 4)
        elif y_pos == len(_map) - 2:
            exit_cells = [3]

        res = [len(exit_cells) != 0 and rotation in exit_cells]
        for item in exit_cells:
            if not _map[y_pos][x_pos][item]:
                res.append(item)
        return res

    def _next_cell(self, x_pos, y_pos, rotation):
        _map = self._scopes['__global']['_map'][1]
        assert _map[y_pos][x_pos]

        exit_cells = self._rotation_to_exit(x_pos, y_pos, rotation)
        if rotation in exit_cells[1:]:
            return [-1, -1]
        if exit_cells[0]:

            return [x_pos, y_pos]

        if rotation == 0:
            return [x_pos, y_pos - 2]
        if rotation == 3:
            return [x_pos, y_pos + 2]

        if y_pos % 2 == 0:
            if rotation == 1:
                return [x_pos, y_pos - 1]
            elif rotation == 2:
                return [x_pos, y_pos + 1]
            elif rotation == 4:
                return [x_pos - 1, y_pos + 1]
            else:
                return [x_pos - 1, y_pos - 1]

        if y_pos % 2 == 1:
            if rotation == 1:
                return [x_pos + 1, y_pos - 1]
            elif rotation == 2:
                return [x_pos + 1, y_pos + 1]
            elif rotation == 4:
                return [x_pos, y_pos + 1]
            else:
                return [x_pos, y_pos - 1]

    def _go_op(self):

        _y_pos = self._scopes['__global']['_y_pos'][1]
        _x_pos = self._scopes['__global']['_x_pos'][1]
        _rotation = self._scopes['__global']['_rotation'][1]

        next = self._next_cell(_x_pos, _y_pos, _rotation)
        if next == [-1, -1]:
            self._last_pos = (_x_pos, _y_pos, _rotation)
            self._scopes['__global']['_y_pos'][1] = -1
            self._scopes['__global']['_x_pos'][1] = -1
            self._scopes['__global']['_rotation'][1] = -1
            return 1

        if next == [_x_pos, _y_pos]:
            return 0

        if self._scopes['__global']['_map'][1][_y_pos][_x_pos][_rotation] == 0:
            self._last_pos = (_x_pos, _y_pos, _rotation)
            self._scopes['__global']['_y_pos'][1] = next[1]
            self._scopes['__global']['_x_pos'][1] = next[0]
            return 1

        return 0

    def _rl_op(self):
        y_pos = self._scopes['__global']['_y_pos'][1]
        x_pos = self._scopes['__global']['_x_pos'][1]
        rotation = self._scopes['__global']['_rotation'][1]
        self._last_pos = (x_pos, y_pos, rotation)

        if rotation == 0:
            self._scopes['__global']['_rotation'][1] = 5
        else:
            self._scopes['__global']['_rotation'][1] -= 1

    def _rr_op(self):
        y_pos = self._scopes['__global']['_y_pos'][1]
        x_pos = self._scopes['__global']['_x_pos'][1]
        rotation = self._scopes['__global']['_rotation'][1]
        self._last_pos = (x_pos, y_pos, rotation)


        if rotation == 5:
            self._scopes['__global']['_rotation'][1] = 0
        else:
            self._scopes['__global']['_rotation'][1] += 1

    def _unwrap_list(self, arr):
        if type(arr) == list and len(arr) == 1:
            while type(arr[0]) == list and len(arr) == 1:
                arr = arr[0]
                if type(arr[0]) == int:
                    break
            return arr
        elif type(arr) == int:
            return arr
        else:
            res = []
            for item in arr:
                res_list = self._unwrap_list(item)
                if type(res_list) == list and len(res_list) > 1 and type(res_list[0]) == list:
                    for item in res_list:
                        res.append(item)
                else:
                    res.append(res_list)
            return res

    def _sonar_op(self):

        y_pos = self._scopes['__global']['_y_pos'][1]
        x_pos = self._scopes['__global']['_x_pos'][1]
        rotation = self._scopes['__global']['_rotation'][1]

        excluded = (rotation + 3) % 6
        if self._last_pos == (x_pos, y_pos, rotation):
            self._sonar_calls += 1
        else:
            self._sonar_calls = 1
        self._last_pos = (x_pos, y_pos, rotation)
        res_list = self._sonar(x_pos, y_pos, 1, excluded, -1, -1)
        res_list = self._unwrap_list(res_list)

        return res_list

    def _sonar(self, x_pos, y_pos ,level, excluded, last_x, last_y):
        _map = self._scopes['__global']['_map'][1]
        res = [i for i in range(0, 6)]
        res_value = []
        if level == self._sonar_calls:
            for visit in range(len(res)):
                if visit == excluded and level == 1:
                    res_value.append(-1)
                    continue
                try:
                    res_value.append(_map[y_pos][x_pos][visit])
                except IndexError:
                    continue
            return res_value

        visit_list = []
        for visit in range(len(res)):
            next = self._next_cell(x_pos, y_pos, visit)
            if _map[y_pos][x_pos][visit] == 0 and next != [x_pos, y_pos] and next != [last_x, last_y]:
                res_list = self._sonar(next[0], next[1], level + 1, excluded, x_pos, y_pos)
                if type(res_list) == list and len(res_list) == 1 and type(res_list[0]) == list:
                    visit_list.append(res_list[0])
                else:
                    visit_list.append(res_list)
        return visit_list

    def _get_pos(self):
        y_pos = self._scopes['__global']['_y_pos'][1]
        x_pos = self._scopes['__global']['_x_pos'][1]
        rotation = self._scopes['__global']['_rotation'][1]
        return x_pos, y_pos, rotation

    def _get_neighbours(self):
        _map = self._scopes['__global']['_map'][1]
        neighbours = []
        pos = self._get_pos()

        for visit in range(6):

            if _map[pos[1]][pos[0]][visit] == 0:
                candidate = [self._next_cell(pos[0], pos[1], visit), visit]
                if candidate != [[pos[0], pos[1]], visit]:
                    neighbours.append(candidate)
        return neighbours

    def _compass_op(self):

        _map = self._scopes['__global']['_map'][1]

        start_pos = self._get_pos()
        last_pos = self._last_pos
        for i in range(6):
            if self._next_cell(start_pos[0], start_pos[1], i) == [-1, -1]:
                return i
        marked = {(start_pos[0], start_pos[1]), (-1, -1)}
        visit_stack = []
        path = [(start_pos[0], start_pos[1])]

        map_size = len(_map[0]) * len(_map) + 1
        all_paths = []

        while True:
            flag = False
            for item in self._get_neighbours():
                if (item[0][0], item[0][1]) not in marked:
                    visit_stack.append(self._get_pos())

                    self._scopes['__global']['_rotation'][1] = item[1]
                    self._go_op()

                    marked.add((self._get_pos()[0],self._get_pos()[1]))
                    path.append((self._get_pos()[0], self._get_pos()[1]))
                    flag = True
                    break
                elif item[0] == [-1, -1]:
                    #visit_stack.append(self._get_pos())
                    #path.append((self._get_pos()[0], self._get_pos()[1]))
                    self._scopes['__global']['_x_pos'][1] = -1
                    self._scopes['__global']['_y_pos'][1] = -1
                    self._scopes['__global']['_rotation'][1] = -1
                    flag = True
                    break
            if not flag and len(visit_stack):
                pos = visit_stack.pop(-1)
                self._scopes['__global']['_x_pos'][1] = pos[0]
                self._scopes['__global']['_y_pos'][1] = pos[1]
                self._scopes['__global']['_rotation'][1] = pos[2]
                path.pop(-1)

            if self._get_pos() == (-1, -1, -1):
                #path.pop(-1)
                if not self._check_dead_end(path[-1]):
                    all_paths.append(copy.deepcopy(path))
                if len(marked) == map_size:
                    break
                pos = visit_stack.pop(-1)
                self._scopes['__global']['_x_pos'][1] = pos[0]
                self._scopes['__global']['_y_pos'][1] = pos[1]
                self._scopes['__global']['_rotation'][1] = pos[2]

            if len(marked) == map_size:
                if not self._check_dead_end(path[-1]):
                    all_paths.append(copy.deepcopy(path))
                break

        # for item in all_paths:
        #     print(item)

        self._scopes['__global']['_x_pos'][1] = start_pos[0]
        self._scopes['__global']['_y_pos'][1] = start_pos[1]
        self._scopes['__global']['_rotation'][1] = start_pos[2]
        self._last_pos = last_pos

        shortest = min(all_paths, key=lambda x: len(x))
        #print(shortest)

        #print("pos: ", self._get_pos())
        #print("next: ", self._next_cell(0, 2, 5))

        return self._calculate_dir([shortest[-1][0], shortest[-1][1]])

    def _calculate_dir(self, finish):
        _map = self._scopes['__global']['_map'][1]
        y_pos = self._scopes['__global']['_y_pos'][1]
        x_pos = self._scopes['__global']['_x_pos'][1]
        last_pos = []
        cur_pos = [x_pos, y_pos]
        all_paths = []
        for i in range(6):
            path = [cur_pos]
            cnt = 0

            if cur_pos[1] == 0:
                if cur_pos[0] == 0:
                    if i in (0, 1, 4, 5):
                        continue
                else:
                    if i in (0, 1, 5):
                        continue
            elif cur_pos[1] == 1:
                if cur_pos[0] == len(_map[0]) - 1:
                    if i in (0, 1, 2):
                        continue
                else:
                    if i == 0:
                        continue

            elif cur_pos[1] == len(_map) - 1:
                if cur_pos[0] == 0:
                    if i in (2, 3, 4, 5):
                        continue
                if i in (2, 3, 4):
                    continue
            elif cur_pos[0] == len(_map[0]) - 1:
                if i in (1, 2):
                    continue
            elif cur_pos[1] == len(_map) - 2:
                if i == 3:
                    continue
            elif cur_pos[0] == 0:
                if i in (4, 5):
                    continue

            while cur_pos != [-1, -1] and cur_pos != last_pos:
                last_pos = cur_pos
                cur_pos = self._next_cell(last_pos[0], last_pos[1], i)
                path.append(cur_pos)

            if cur_pos == last_pos and len(path) != 1:
                path.pop(-1)
            elif cur_pos == [-1, -1]:
                path.pop(-1)
                cur_pos = path[-1]

            while cur_pos != finish:
                if cur_pos[1] == 0:
                    cur_pos = self._next_cell(cur_pos[0], cur_pos[1], 2)
                    cnt += 1
                    continue
                elif cur_pos[1] == 1 and cur_pos[0] != len(_map[0]) - 1:
                    cur_pos = self._next_cell(cur_pos[0], cur_pos[1], 1)
                    cnt += 1
                    continue
                elif cur_pos[1] == len(_map) - 1 and cur_pos[0] != 0:
                    cur_pos = self._next_cell(cur_pos[0], cur_pos[1], 5)
                    cnt += 1
                    continue
                elif cur_pos[1] == len(_map) - 2:
                    cur_pos = self._next_cell(cur_pos[0], cur_pos[1], 4)
                    cnt += 1
                    continue

                elif cur_pos[0] == 0:
                    cur_pos = self._next_cell(cur_pos[0], cur_pos[1], 0)
                    cnt += 1
                    continue

                elif cur_pos[0] == len(_map[0]) - 1:
                    cur_pos = self._next_cell(cur_pos[0], cur_pos[1], 3)
                    cnt += 1
                    continue

            cnt = min(cnt, abs(25 - cnt))

            all_paths.append((i, cnt + len(path)))
            cur_pos = [x_pos, y_pos]
            last_pos = []

        return min(all_paths, key=lambda x: x[1])[0]

    def _check_dead_end(self, pos):
        for i in range(6):
            if self._next_cell(pos[0], pos[1], i) == [-1, -1]:
                return False
        return True

    def visit(self, node, scope_name='__global'):
        method = 'visit_' + node.__class__.__name__
        return getattr(self, method, self.generic_visit)(node, scope_name=scope_name)

    def generic_visit(self, node, scope_name='__global'):
        if node is None:
            return None
        else:
            return (self.visit(c, scope_name=scope_name) for c_name, c in node.children())

    def visit_Constant(self, n, scope_name='__global'):
        return int(n.value)

    def visit_ID(self, n, scope_name='__global'):

        ref = self._scopes[scope_name].get(n.name)
        if not ref:
            ref = self._scopes['__global'].get(n.name)
            if not ref:
                print("Error at {}: unresolved reference to {}".format(n.coord, n.name))
                self._error_flag = True
                return None

        return n.name

    def visit_ArrayRef(self, n, scope_name='__global'):
        indices = []
        cur = n
        if type(cur.subscript) == ast.BinaryOp:
            while type(cur) == ast.ArrayRef:
                indices.append(self.visit(cur.subscript, scope_name))
                cur = cur.name

        elif type(cur.subscript) != ast.ID:
            while type(cur) == ast.ArrayRef:
                indices.append(int(cur.subscript.value))
                cur = cur.name
        else:
            ref = self._scopes[scope_name].get(cur.subscript.name)
            if not ref:
                ref = self._scopes['__global'].get(cur.subscript.name)
                if not ref:
                    print("Error at {}: unresolved reference to {}".format(n.coord, cur.subscript.name))
                    self._error_flag = True
                    return None
            while type(cur) == ast.ArrayRef:
                indices.append(ref[1])
                cur = cur.name

        ref = self._scopes[scope_name].get(cur.name)
        if not ref:
            ref = self._scopes['__global'].get(cur.name)
            if not ref:
                print("Error at {}: unresolved reference to {}".format(n.coord, cur.name))
                self._error_flag = True
                return None


        res = ref[1]
        if res is None:
            return
        if type(res[0]) == list and len(indices) == 1:
            res = res[0]


        for index in reversed(indices):
            try:
                res = res[index]
            except (IndexError, TypeError):
                print("Error at {}: invalid index".format(n.coord))
                self._error_flag = True
                return

        return res

    def _build_print_args(self, name, arr, scope_name, n, flag=False):
        if type(name) == list:
            if not flag:
                arr.append(str(name))
            # else:  # TODO check list for correct types
            #     arr.append(name)
            return

        ref = self._scopes[scope_name].get(name)
        if not ref:
            ref = self._scopes['__global'].get(name)
            if not ref:
                print("Error at {}: unresolved reference to {}".format(n.coord, name))
                self._error_flag = True
                return
        if not flag:
            arr.append(str(ref[1]))
        else:
            arr.append(ref[1])

    def visit_FuncCall(self, n, scope_name='__global'):
        if n.name.name == 'print':
            val = self.visit(n.args, scope_name=scope_name)
            s = []
            if type(val) == list:
                for item in val:
                    if item is None:
                        return
                    if type(item) == int or type(item) == bool:
                        s.append(str(item))
                        continue

                    self._build_print_args(item, s, scope_name, n)
            elif type(val) == str:
                self._build_print_args(val, s, scope_name, n)
            else:
                s.append(str(val))
            if not self._error_flag:
                print("PRINT CALL: ", ', '.join(s))
        else:
            old_scope = scope_name
            if not self._error_flag:
                args_values = self.visit(n.args, scope_name=scope_name)
                scope_name = n.name.name
                i = 1
                if not self._scopes.get(scope_name + str(i)):
                    self._scopes[scope_name + str(i)] = copy.deepcopy(self._scopes[scope_name])
                else:
                    while self._scopes.get(scope_name + str(i)):
                        i += 1
                    self._scopes[scope_name + str(i)] = copy.deepcopy(self._scopes[scope_name])
                scope_name = scope_name + str(i)
                s = []
                if args_values:
                    if self._func_dict[n.name.name].decl.type.args is None:
                        print("Error at {}: function {} has no parameters".format(n.coord, n.name.name))
                        self._error_flag = True
                        return
                    for arg in args_values:
                        if type(arg) == int or type(arg) == bool:
                            s.append(arg)
                        else:
                            self._build_print_args(arg, s, old_scope, n, True)
                    if not(type(s[0]) == list and len(s[0]) == 1):
                        s = [int(el) for el in s]
                    for j, item in enumerate([val.type.declname for val in self._func_dict[n.name.name].decl.type.args.params]):

                        self._scopes[scope_name][item][1] = s[j]
                else:
                    if self._func_dict[n.name.name].decl.type.args:
                        print("Error at {}: not enough parameters for {}".format(n.coord, n.name.name))
                        self._error_flag = True
                        return
                res = self.visit_Compound(self._func_dict[n.name.name].body, scope_name, execute_flag=True)

                del self._scopes[scope_name]

                return res

    def visit_UnaryOp(self, n, scope_name='__global'):
        if n.op in self.robot_operators.keys():
            if n.expr:
                print("Error at {}: invalid usage of robot operator {}".format(n.coord, n.op))
                self._error_flag = True
            if not self._error_flag:
                return self.robot_operators[n.op]()
        else:
            if type(n.expr) == ast.ID:
                ref = self._scopes[scope_name].get(n.expr.name)
                if not ref:
                    ref = self._scopes['__global'].get(n.expr.name)
                    if not ref:
                        print("Error at {}: unresolved reference to {}".format(n.coord, n.expr.name))
                        self._error_flag = True
                        return
                return self.unary_operators[n.op](ref[1])
            return self.unary_operators[n.op](self.visit(n.expr, scope_name=scope_name))

    def visit_BinaryOp(self, n, scope_name='__global'):

        ref_l = lvalue = self.visit(n.left, scope_name=scope_name)
        if self._error_flag:
            return
        if type(lvalue) == str:
            ref_l = self._scopes[scope_name].get(lvalue)
            if not ref_l:
                ref_l = self._scopes['__global'].get(lvalue)
                if not ref_l:
                    print("Error at {}: unresolved reference to {}".format(n.coord, lvalue))
                    self._error_flag = True
                    return

            ref_l = ref_l[1]

        ref_r = rvalue = self.visit(n.right, scope_name=scope_name)
        if type(rvalue) == str:
            ref_r = self._scopes[scope_name].get(rvalue)
            if not ref_r:
                ref_r = self._scopes['__global'].get(rvalue)
                if not ref_r:
                    print("Error at {}: unresolved reference to {}".format(n.coord, rvalue))
                    self._error_flag = True
                    return

            ref_r = ref_r[1]
        return self.operators[n.op](ref_l, ref_r)

    def visit_Assignment(self, n, scope_name='__global'):

        if type(n.lvalue) == ast.ArrayRef:

            indices = []
            cur = n.lvalue
            if type(cur.subscript) == ast.BinaryOp:
                while type(cur) == ast.ArrayRef:
                    indices.append(self.visit(cur.subscript, scope_name))
                    cur = cur.name
            elif type(cur.subscript) == ast.ID:
                ref = self._scopes[scope_name].get(cur.subscript.name)
                if not ref:
                    ref = self._scopes['__global'].get(cur.subscript.name)
                    if not ref:
                        print("Error at {}: unresolved reference to {}".format(n.coord, cur.subscript.name))
                        self._error_flag = True
                        return None
                while type(cur) == ast.ArrayRef:
                    indices.append(ref[1])
                    cur = cur.name

            else:
                if cur.subscript is None:
                    return
                while type(cur) == ast.ArrayRef:

                    indices.append(int(cur.subscript.value))
                    cur = cur.name

            ref = self._scopes[scope_name].get(cur.name)
            if not ref:
                ref = self._scopes['__global'].get(cur.name)
                if not ref:
                    print("Error at {}: unresolved reference to {}".format(n.coord, cur.name))
                    self._error_flag = True
                    return

            rvalue = self.visit(n.rvalue, scope_name=scope_name)

            if type(rvalue) == str:
                rvalue = self._scopes[scope_name].get(rvalue)
                if not rvalue:
                    rvalue = self._scopes['__global'].get(rvalue)
                    if not rvalue:
                        print("Error at {}: unresolved reference to {}".format(n.coord, cur.name))
                        self._error_flag = True
                        return
                rvalue = rvalue[1]

            res = ref[1]

            if type(res[0]) == list and len(res) == 1 and len(indices) == 1:
                res = res[0]

            copy_ind = copy.copy(indices)
            for index in reversed(indices):
                try:
                    last = res
                    res = res[index]
                    copy_ind.pop(-1)
                    if type(res) == int:
                        if type(rvalue) == list:
                            print("Error at {}: invalid index".format(n.coord))
                            self._error_flag = True
                            return
                        if not self._error_flag:
                            if len(copy_ind) > 0:
                                print("Error at {}: invalid index".format(n.coord))
                                self._error_flag = True
                                return
                            last[index] = rvalue
                        return rvalue

                except (IndexError, TypeError):
                    print("Error at {}: invalid index".format(n.coord))
                    self._error_flag = True
                    return

            if len(last[indices[0]]) != rvalue:
                print("Error at {}: invalid field".format(n.coord))
                self._error_flag = True
                return
            if not self._error_flag:
                last[indices[0]] = rvalue
                return rvalue
            return

        lvalue = self.visit(n.lvalue, scope_name=scope_name)
        rvalue = self.visit(n.rvalue, scope_name=scope_name)

        ref = self._scopes[scope_name].get(lvalue)
        if not ref:
            ref = self._scopes['__global'].get(lvalue)
            if not ref:
                print("Error at {}: unresolved reference to {}".format(n.coord, lvalue))
                self._error_flag = True
                return

        if type(rvalue) == str:
            ref_rvalue = self._scopes[scope_name].get(rvalue)
            if not ref_rvalue:
                ref_rvalue = self._scopes['__global'].get(rvalue)
                if not ref_rvalue:
                    print("Error at {}: unresolved reference to {}".format(n.coord, rvalue))
                    self._error_flag = True
                    return
            if type(lvalue) == str:
                ref_lvalue = self._scopes[scope_name].get(lvalue)
                if not ref_lvalue:
                    ref_lvalue = self._scopes['__global'].get(lvalue)
                    if not ref_lvalue:
                        print("Error at {}: unresolved reference to {}".format(n.coord, lvalue))
                        self._error_flag = True
                        return
                ref_lvalue[1] = ref_rvalue[1]
                return lvalue
            lvalue = ref_rvalue[1]
            return ref_rvalue[1]

        if not self._error_flag:
            if type(rvalue) == list and type(rvalue[0]) != list:
                ref[1] = [rvalue]
                # TODO check types when field small tiny << sonar
                return [rvalue]
            try:
                while not self.borders[ref[0]](rvalue):
                    if ref[0] == 'big' and not self.borders[ref[0]](rvalue):
                        print("Error at {}: too big value for {}".format(n.coord, lvalue))
                        self._error_flag = True
                        return
                    ref[0] = self.casts[ref[0]]
            except (KeyError, TypeError):

                try:
                    while not self.u_borders[ref[0]](rvalue):
                        if ref[0] == 'big' and not self.borders[ref[0]](rvalue):
                            print("Error at {}: too big value for {}".format(n.coord, lvalue))
                            self._error_flag = True
                            return

                        ref[0] = self.casts[ref[0]]
                except (KeyError, TypeError):
                    print("Error at {}: cannot cast {}".format(n.coord, lvalue))
                    self._error_flag = True
                    return


            ref[1] = rvalue
            return rvalue




    def _visit_expr(self, n, scope_name='__global'):

        return self.visit(n, scope_name=scope_name)

    def _check_levels(self, arr, level, num):
        if level == 0:
            return len(arr) == num
        level -= 1
        res = True
        for item in arr:
            res &= self._check_levels(item, level, num)
        return res

    def _check_rows(self, arr, list_of_lists, level):
        res = True
        while arr[0] == 'field':
            res &= self._check_levels(list_of_lists, level, self.field_size[arr[-1][0]])
            level += 1
            self._check_rows(arr[1], list_of_lists, level)
            return res

    def _build_columns(self, arr, level, num, init=None):
        if level == 0:
            for _ in range(num):
                if init is None:
                    arr.append([])
                else:
                    arr.append(init)
            return
        level -= 1
        for item in arr:
            self._build_columns(item, level, num, init=init)
        return

    def _build_rows(self, arr, res, num, level=0):
        while type(arr) == list:
            key = arr[-1][0]

            init = None
            if len(key) == 1:
                key = arr[-1]
                init = num
            self._build_columns(res, level, self.field_size[key], init=init)
            level += 1
            if len(arr) == 1:
                return
            self._build_rows(arr[1], res, num, level)
            return

    def visit_Decl(self, n, scope_name='__global'):


        if self._scopes[scope_name].get(n.name):
            print("Error at {}: redefinition of variable {} in {} scope".format(n.coord, n.name, scope_name))
            self._error_flag = True
        val = self.visit(n.init, scope_name=scope_name)
        if n.type.type.names[0] == 'field':

            if type(n.init) == ast.InitList or type(val) == list:
                if type(val) == list and type(val[0]) != list:
                    val = [val]
                try:
                    if not self._check_rows(n.type.type.names, val, 0):
                        print("Error at {}: incorrect field declaration".format(n.coord))
                        self._error_flag = True
                except TypeError:
                    print("Error at {}: incorrect field declaration".format(n.coord))
                    self._error_flag = True
            elif type(n.init) == ast.ID:
                if self._scopes[scope_name].get(n.init.name) is None:
                    if self._scopes['__global'].get(n.init.name) is None:
                        print("Error at {}: unresolved reference to variable {}".format(n.coord, n.init.name))
                        self._error_flag = True
                    else:
                        val = self._scopes['__global'][n.init.name][1]
                elif n.type.type.names != self._scopes[scope_name][n.init.name][0]:
                    print("Error at {}: type {} doesn't correlate with {}".format(n.coord, n.type.type.names, n.init.name))
                    self._error_flag = True
                else:
                    val = self._scopes[scope_name][n.init.name][1]
            elif val is None:
                return
            elif type(n.init) == ast.UnaryOp and n.init.op in self.robot_operators.keys():
                try:
                    if not self._check_rows(n.type.type.names, val, 0):
                        print("Error at {}: incorrect field declaration".format(n.coord))
                        self._error_flag = True
                except TypeError:
                    print("Error at {}: incorrect field declaration".format(n.coord))
                    self._error_flag = True



            else:
                res = []

                self._build_rows(n.type.type.names, res, val, level=0)
                val = res

            self._scopes[scope_name][n.name] = [n.type.type.names, val]
        else:
            if type(n.init) == ast.ID:
                if self._scopes[scope_name].get(n.init.name) is None:
                    if self._scopes['__global'].get(n.init.name) is None:
                        print("Error at {}: unresolved reference to variable {}".format(n.coord, n.init.name))
                        self._error_flag = True
                    else:
                        val = self._scopes['__global'][n.init.name][1]

                elif n.type.type.names[0] != self._scopes[scope_name][n.init.name][0] and \
                        'u' + n.type.type.names[0] != self._scopes[scope_name][n.init.name][0]:
                    print("Error at {}: type {} doesn't correlate with {}".format(n.coord, n.type.type.names[0], n.init.name))
                    self._error_flag = True
                else:
                    n.type.type.names[0] = self._scopes[scope_name][n.init.name][0]
                    val = self._scopes[scope_name][n.init.name][1]
            elif val is None:
                return
            elif n.type.type.names[0] == 'tiny':
                if type(val) == bool or val == 0 or val == 1:
                    val = int(val)
                else:
                    print("Error at {}: incorrect declaration of {}".format(n.coord, n.name))
                    self._error_flag = True

            elif isinstance(n.init, (ast.UnaryOp, ast.BinaryOp)):
                # if n.init.op in self.robot_operators.keys():
                #     print("Error at {}: incorrect declaration of {}".format(n.coord, n.name))
                #     self._error_flag = True
                #     return
                if not self.borders[n.type.type.names[0]](val):
                    print("Error at {}: incorrect declaration of {}".format(n.coord, n.name))
                    self._error_flag = True
            elif type(n.init) == ast.InitList:
                print("Error at {}: type {} cannot be initialized by init list".format(n.coord, n.type.type.names[0]))
                self._error_flag = True
            elif type(n.init) == ast.ArrayRef:
                if type(val) == list or not self.borders[n.type.type.names[0]](val):
                    print("Error at {}: incorrect declaration of {}".format(n.coord, n.name))
                    self._error_flag = True
            else:
                if not self.u_borders[n.type.type.names[0]](val):
                    print("Error at {}: incorrect declaration of {}".format(n.coord, n.name))
                    self._error_flag = True
                if n.type.type.names[0][0] != 'u':
                    n.type.type.names[0] = 'u' + n.type.type.names[0]

            self._scopes[scope_name][n.name] = [n.type.type.names[0], val]

    def visit_ExprList(self, n, scope_name='__global'):
        visited_subexprs = []
        for expr in n.exprs:
            visited_subexprs.append(self._visit_expr(expr, scope_name=scope_name))
        return visited_subexprs

    def visit_InitList(self, n, scope_name='__global'):
        visited_subexprs = []
        for expr in n.exprs:
            visited_subexprs.append(self.visit(expr))
        return visited_subexprs

    def visit_FuncDef(self, n, scope_name='__global'):
        self._scopes[n.decl.name] = {}
        if n.decl.type.args:
            for item in n.decl.type.args.params:  # Add parameters to function scope
                if item.type.type.names[0] == 'field':
                    self._scopes[n.decl.name][item.type.declname] = [item.type.type.names, None]
                else:
                    self._scopes[n.decl.name][item.type.declname] = [item.type.type.names[0], None]


        #print(self._scopes)

        exec_flag = False
        if n.decl.name == 'main':  # Starting to execute
            exec_flag = True

        self.visit_Compound(n.body, func_name=n.decl.name, execute_flag=exec_flag)
        if not exec_flag:


            for item in copy.copy(self._scopes[n.decl.name]).keys():
                if n.decl.type.args is None:
                    del self._scopes[n.decl.name][item]
                elif item not in [val.type.declname for val in n.decl.type.args.params]:
                    del self._scopes[n.decl.name][item]

        print(self._scopes)

    def visit_FileAST(self, n, scope_name='__global'):

        for ext in n.ext:

            self.visit(ext)

    def visit_Compound(self, n, func_name=None, execute_flag=False):

        if n.block_items:
            for item in n.block_items:
                if execute_flag and not self._error_flag:
                    if type(item) == ast.Return:
                        return self.visit(item, scope_name=func_name)
                    elif type(item) == ast.If:
                        res = self.visit_If(item, scope_name=func_name, execute_flag=execute_flag)
                        if res is not None:
                            return res
                    elif type(item) == ast.While:
                        res = self.visit_While(item, scope_name=func_name)
                        if res is not None:
                            return res
                    elif type(item) == ast.FuncCall:
                        self.visit(item, scope_name=func_name)
                    elif type(item) in (ast.ID, ast.Constant):
                        print("Error at {}: {} without target occured".format(item.coord, type(item)))
                        self._error_flag = True
                        continue

                if not isinstance(item, (ast.If, ast.While, ast.Return, ast.FuncCall)):
                    if type(item) in (ast.ID, ast.Constant):
                        print("Error at {}: {} without target occured".format(item.coord, type(item)))
                        self._error_flag = True
                        continue
                    self.visit(item, func_name)

    def visit_EmptyStatement(self, n, scope_name='__global'):
        # print("Error at {}: empty statement occured".format(n.coord))
        # self._error_flag = True
        return

    def visit_ParamList(self, n):
        return [].extend(self.visit(param) for param in n.params)

    def visit_Return(self, n, scope_name='__global'):
        if type(n.expr) == ast.ID:

            ref = self._scopes[scope_name].get(n.expr.name)
            if not ref:
                ref = self._scopes['__global'].get(n.expr.name)
                if not ref:
                    print("Error at {}: unresolved reference to {}".format(n.coord, n.expr.name))
                    self._error_flag = True
                    return None
                else:
                    scope_name = '__global'
            return ref[1]
        else:
            return self.visit(n.expr, scope_name=scope_name)

    def visit_Break(self, n, scope_name='__global'):
        # TODO break + continue
        return '__break'

    def visit_Continue(self, n, scope_name='__global'):
        return '__continue'

    def visit_If(self, n, scope_name='__global', execute_flag=False):
        if self._error_flag or not execute_flag:
            pass
        if self.visit(n.cond, scope_name=scope_name):
            if type(n.iftrue) == ast.Compound:
                return self.visit_Compound(n.iftrue, scope_name, True)
            else:
                return self.visit(n.iftrue, scope_name)

    def visit_While(self, n, scope_name='__global'):
        cnt = 0
        while self.visit(n.cond, scope_name=scope_name):
            if cnt == 1000000:
                print("Runtime error at {}".format(n.coord))
                self._error_flag = True
                return
            cnt += 1
            if type(n.stmt) == ast.Compound:
                ret = self.visit_Compound(n.stmt, func_name=scope_name, execute_flag=True)
            else:
                ret = self.visit(n.stmt, scope_name)
            if ret == '__continue':
                continue
            elif ret == '__break':
                return
            elif ret is not None:
                return ret
