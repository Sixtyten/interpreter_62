big init() begin
    _y_pos << 10.
    _x_pos << 0.
    _rotation << 0.
end.

tiny check_sonar(field small tiny f) begin
    small cnt, i << 0.
    until i != 6 do begin

        check f[i] == 0 do
            cnt << cnt + 1.
        i << i + 1.
    end.
    return cnt.
end.

small get_index(field small tiny f, small num) begin
    small i << 0.
    until i != 6 do begin

        check f[i] == num do
            return i.
        i << i + 1.
    end.
end.

tiny reverse() begin
    rr.
    rr.
    rr.
end.

small abs(small num) begin
    check num > 0 do
        return num.
    return -num.
end.

small get_nearest(field small tiny f, small dir) begin
    small cnt, i, res << 0.
    small min << 6.

    until i != 6 do begin
        check f[i] == 0 do begin
            check abs(dir - i) < min do begin
                res << i.
                min << abs(dir - i).
            end.
        end.
        i << i + 1.
    end.

    return res.
end.

tiny check_start() begin
    reverse().
    check go and _x_pos == -1 and _y_pos == -1 do
        return 1.
    reverse().
    go.
    return 0.
end.

big main() begin
    init().

    check check_start() do begin
        print(_x_pos, _y_pos).
        return 0.
    end.

    small cnt, index, dir << 0.
    field small tiny t << 0.
    until _x_pos != -1 and _y_pos != -1 do begin
        print(_x_pos, _y_pos, compass).
        t << sonar.
        cnt << check_sonar(t).

        check cnt == 1 do begin
            index << get_index(t, 0).
            until _rotation != index do
                rr.
        end.

        check cnt == 0 do begin
            reverse().
        end.

        check cnt > 1 do begin
            dir << compass.
            dir << get_nearest(t, dir).
            until _rotation != dir do
                rr.
        end.
        go.
    end.
    print(_x_pos, _y_pos).
end.

